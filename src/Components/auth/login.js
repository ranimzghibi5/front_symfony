import React, { useState } from 'react';
import { useNavigate } from 'react-router-dom';
import axios from 'axios';

function Login() {
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');
  const navigate = useNavigate();

  const handleSubmit = async (event) => {
  event.preventDefault();

  try {
    const response = await axios.post('http://localhost:3001/login', { username, password });

    if (response.status === 200) {
      // Authentification réussie
      navigate('/ajout');
    }
  } catch (error) {
    if (error.response && error.response.status === 401) {
      // Identifiants incorrects
      alert('Identifiants incorrects');
    } else {
      console.error('An error occurred while logging in:', error);
    }
  }
};

  return (
    <form onSubmit={handleSubmit}>
      <input type="text" value={username} onChange={e => setUsername(e.target.value)} placeholder="Username" required />
      <input type="password" value={password} onChange={e => setPassword(e.target.value)} placeholder="Password" required />
      <button type="submit">Login</button>
    </form>
  );
}

export default Login;