import React, { useState, useEffect } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import axios from 'axios';

function Ajout() {
  const [students, setStudents] = useState([]);
  const [formData, setFormData] = useState({
    nom: '',
    prenom: '',
    date_de_naissance: ''
  });

  const [editingStudent, setEditingStudent] = useState(null);

  const handleEdit = (student) => {
    setEditingStudent(student);
    setFormData(student);
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    try {
      if (editingStudent) {
        const response = await axios.put(`http://localhost:3001/etudiants/${editingStudent.id}`, formData);
        setStudents(students.map(student => student.id === editingStudent.id ? response.data : student));
        setEditingStudent(null);
      } else {
        const response = await axios.post('http://localhost:3001/etudiants', formData);
        setStudents([...students, response.data]);
      }
      setFormData({
        nom: '',
        prenom: '',
        date_de_naissance: ''
      });
    } catch (error) {
      console.error('Erreur lors de l\'ajout ou de la modification de l\'étudiant : ', error);
    }
  };
  
  const handleDelete = async (student) => {
    try {
      await axios.delete(`http://localhost:3001/etudiants/${student.id}`);
      setStudents(students.filter((s) => s.id !== student.id));
    } catch (error) {
      console.error('Erreur lors de la suppression de l\'étudiant : ', error);
    }
  };

  useEffect(() => {
    const fetchStudents = async () => {
      try {
        const response = await axios.get('http://localhost:3001/etudiants');
        setStudents(response.data);
      } catch (error) {
        console.error('Erreur lors de la récupération des étudiants : ', error);
      }
    };

    fetchStudents();
  }, []);

  const handleChange = (e) => {
    setFormData({ ...formData, [e.target.name]: e.target.value });
  };

  return (
    <div className="container">
        <h1 style={{ color: '#333', textAlign: 'center', fontSize: '2.5em', marginTop: '20px', marginBottom: '20px' }}>Gestion des etudiants</h1>
      <form onSubmit={handleSubmit} className="mb-3">
        <div className="mb-3">
          <label className="form-label">Nom</label>
          <input type="text" name="nom" value={formData.nom} onChange={handleChange} className="form-control" required />
        </div>
        <div className="mb-3">
          <label className="form-label">Prénom</label>
          <input type="text" name="prenom" value={formData.prenom} onChange={handleChange} className="form-control" required />
        </div>
        <div className="mb-3">
          <label className="form-label">Date de naissance</label>
          <input type="date" name="date_de_naissance" value={formData.date_de_naissance} onChange={handleChange} className="form-control" required />
        </div>
        <button type="submit" className="btn btn-primary">{editingStudent ? 'Modifier l\'étudiant' : 'Ajouter l\'étudiant'}</button>
      </form>
      <table className="table">
        <thead>
          <tr>
            <th>Nom</th>
            <th>Prénom</th>
            <th>Date de naissance</th>
            <th></th>
            <th></th>
          </tr>
        </thead>
        <tbody>
          {students.map((student, index) => (
            <tr key={index}>
              <td>{student.nom}</td>
              <td>{student.prenom}</td>
              <td>{student.date_de_naissance}</td>
              <td><button onClick={() => handleEdit(student)} className="btn btn-warning">Modifier</button></td>
              <td><button onClick={() => handleDelete(student)} className="btn btn-danger">Supprimer</button></td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
}

export default Ajout;