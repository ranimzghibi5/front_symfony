module.exports = (req, res, next) => {
    if (req.method === 'POST' && req.path === '/login') {
      if (req.body.username === 'admin' && req.body.password === 'admin') {
        // Authentification réussie
        res.status(200).json({ token: 'votre-jeton-d-authentification' });
      } else {
        // Identifiants incorrects
        res.status(401).json({ message: 'Identifiants incorrects' });
      }
    } else {
      // Toutes les autres requêtes passent
      next();
    }
  };
  